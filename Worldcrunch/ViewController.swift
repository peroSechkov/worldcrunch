//
//  ViewControllers.swift
//  Worldcrunch
//
//  Created by Miki Dimitrov on 11/28/16.
//  Copyright © 2016 hr.manifest.worldcrunch. All rights reserved.
//

import UIKit
import SnapKit
import Alamofire
import StoreKit

class ViewController: UIViewController {
    
    //MARK: Variables
    var webView: UIWebView!
    var token : String!
    var email = ""
    var password = ""
    var products = [SKProduct]()
    var fromSubscribe = false
    
    //MARK: Life cycle functions
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        setUpConstraints()
        loadUrl(url : Constants.mainUrl)
        SKPaymentQueue.default().add(self)
        requestProductData()
        
        if UserDefaults.standard.object(forKey: "toDate") != nil {
            let toDate = UserDefaults.standard.object(forKey: "toDate") as! NSDate
            
            if NSDate().isGreaterThanDate(dateToCompare: toDate) {
                let alertController = UIAlertController(title: nil, message: "Your subscribtion for the first month has expired, do you want to continue your subsctiption for 10$ per month", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "OK", style: .default, handler: { action in
                    self.buyProduct(self.products[0])
                })
                
                let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                    alertController.dismiss(animated: true, completion: nil)
                })
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                UserDefaults.standard.removeObject(forKey: "toDate")
                self.present(alertController, animated: true, completion: nil)
                
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: SettingUp views and constraints
    func setupViews() {
        webView = UIWebView()
        webView.delegate = self
        self.view.addSubview(webView)
    }
    
    func setUpConstraints() {
        self.navigationController?.navigationBar.isHidden = true
        webView?.snp.makeConstraints { make in
            make.top.equalTo(self.view)
            make.left.equalTo(self.view)
            make.right.equalTo(self.view)
            make.bottom.equalTo(self.view)
        }
    }
    
    func loadUrl(url : String) {
        let weburl = NSURL(string: url)
        let request = NSMutableURLRequest(url: weburl! as URL)
        webView.loadRequest(request as URLRequest)
    }
    func saveDateToUserDefaults() {
        var date = Date()
        var components = DateComponents()
        components.setValue(1, for: .month)
        date = Calendar.current.date(byAdding: components, to: date)!
        UserDefaults.standard.set(date,forKey : "toDate")
    }
    
    func deletePurchase(identifier : String) {
        ApiManager.sharedInstance.userPurchase(identifier: identifier, delete: true,completion: { success in
            if success {
                let alertController = UIAlertController(title: nil, message: "Subscription is deleted", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            } else {
               print("Failure")
            }
        })
      
    }
    //MARK: Helper functions for purchasing a product
    func userPurchase(productIdentifier : String) {
        ApiManager.sharedInstance.userPurchase(identifier: productIdentifier,delete : false,completion: { success in
            if success {
                self.loadUrl(url: Constants.mainUrl)
            } else {
                let alertController = UIAlertController(title: nil, message: "The purchase isn't valid", preferredStyle: .alert)
                let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    func requestProductData() {
        let productSet = NSSet(objects: Constants.productFirstMonth ,Constants.productOneMonth,Constants.productSixMonths,Constants.productTwelveMonths)
        let  request = SKProductsRequest(productIdentifiers: productSet as! Set<String>)
        request.delegate = self
        request.start()
    }
    
    public func buyProduct(_ product: SKProduct) {
        print("Buying \(product.productIdentifier)...")
        let payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
}

//MARK: UIWebViewDelegate functions
extension ViewController : UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let surl = request.url?.absoluteString
        
        if (surl?.contains(Constants.tinyPassUrl))! {
            loadUrl(url : Constants.mainUrl)
            if (surl?.contains(Constants.monthlyOfferId))! {
                buyProduct(products[1])
                saveDateToUserDefaults()
            } else if (surl?.contains(Constants.sixMonthsOfferId))! {
                buyProduct(products[2])
            } else {
                buyProduct(products[3])
            }
            return false
        }
        
        if (surl?.contains("logout"))! {
          UserDefaults.standard.set(nil, forKey: "UserId")
        }
        
        if (surl?.contains("login"))! {
            email = webView.stringByEvaluatingJavaScript(from: "document.querySelectorAll(\"input[type='email']\")[0].value")!
            password = webView.stringByEvaluatingJavaScript(from: "document.querySelectorAll(\"input[type='password']\")[0].value")!
            if email != "" && password != "" {
                ApiManager.sharedInstance.getUserInfo(email: email, password: password, completion: { (success,error,user) -> () in
                    if success {
                        UserDefaults.standard.set(user?.id, forKey: "UserId")
                        if self.fromSubscribe {
                            self.loadUrl(url: Constants.subscribeUrl)
                            self.fromSubscribe = false
                        }
                    } else {
                        let alertController = UIAlertController(title: nil, message: error?.errorDescription, preferredStyle: .alert)
                        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                        alertController.addAction(cancelAction)
                        self.present(alertController, animated: true, completion: nil)
                    }
                })
            }
        }
        
        if (surl?.contains("voucher"))! {
            let alertController = UIAlertController(title: nil, message: "Voucher code is not allowed", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            self.present(alertController, animated: true, completion: nil)
            return false
        }
        
        if (navigationType == .linkClicked){
            if (surl?.contains("subscribe"))! {
                if UserDefaults.standard.object(forKey: "UserId") == nil{
                    loadUrl(url: Constants.loginUrl)
                    fromSubscribe = true
                }
            }
            return true
        }
        return true
    }
}

//MARK: SKProductsRequestDelegate functions
extension ViewController : SKProductsRequestDelegate {
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        self.products = response.products
        print("Loaded list of products...")
        for p in self.products {
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
    }
    
    public func request(_ request: SKRequest, didFailWithError error: Error) {
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
    }
}

//MARK: SKPaymentTransactionObserver functions
extension ViewController: SKPaymentTransactionObserver {
    
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch (transaction.transactionState) {
            case .purchased:
                complete(transaction: transaction)
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
                restore(transaction: transaction)
                break
            case .deferred:
                break
            case .purchasing:
                break
            }
        }
    }
    
    private func complete(transaction: SKPaymentTransaction) {
        print("complete...")
        deliverPurchaseNotificationFor(identifier: transaction.payment.productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
        let productIdentifier = transaction.payment.productIdentifier
        userPurchase(productIdentifier: productIdentifier)
    }
    
    private func restore(transaction: SKPaymentTransaction) {
        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
        print("restore... \(productIdentifier)")
        deliverPurchaseNotificationFor(identifier: productIdentifier)
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    private func fail(transaction: SKPaymentTransaction) {
        print("fail...")
        if let transactionError = transaction.error as? NSError {
            if transactionError.code != SKError.paymentCancelled.rawValue {
                print("Transaction Error: \(transaction.error?.localizedDescription)")
            }
        }
        SKPaymentQueue.default().finishTransaction(transaction)
    }
    
    private func deliverPurchaseNotificationFor(identifier: String?) {
        guard let identifier = identifier else { return }
        UserDefaults.standard.set(true, forKey: identifier)
        UserDefaults.standard.synchronize()
    }
}

extension NSDate {
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        var isGreater = false
        
        if self.compare(dateToCompare as Date) == ComparisonResult.orderedDescending {
            isGreater = true
        }
        return isGreater
    }
}
