//
//  Constants.swift
//  Worldcrunch
//
//  Created by Miki Dimitrov on 11/28/16.
//  Copyright © 2016 hr.manifest.worldcrunch. All rights reserved.
//
import Foundation

struct Constants {
    
    static let monthlyOfferId = "889-792545649"
    
    static let sixMonthsOfferId = "889-641041636"
    
    static let twelveMonthsOfferId = "889-1120709593"
    
    static let subscribeUrl = "http://www.worldcrunch.com/subscribe"
    
    static let loginUrl = "http://www.worldcrunch.com/login"
    
    static let mainUrl = "http://www.worldcrunch.com"
    
    static let tinyPassUrl = "https://buy.tinypass.com/tkt/ps/choosePaymentMethod?"
    
    //MARK: iPhone
    
    static let productFirstMonth = "hr.manifest.worldcrunch.1"
    
    static let productOneMonth = "hr.manifest.worldcrunch"
    
    static let productSixMonths = "hr.manifest.worldcrunch.2"
    
    static let productTwelveMonths = "hr.manifest.worldcrunch.3"
    
    //MARK: iPad
//    static let productOneMonth = "hr.manifest.worldcrunchipad"
//    
//   static let productFirstMonth = "hr.manifest.worldcrunchipad.1"    
//    static let productSixMonths = "hr.manifest.worldcrunchipad.2"
//    
//    static let productTwelveMonths = "hr.manifest.worldcrunchipad.3"
    
}
