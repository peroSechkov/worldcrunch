//
//  Router.swift
//  Worldcrunch
//
//  Created by Miki Dimitrov on 11/28/16.
//  Copyright © 2016 hr.manifest.worldcrunch. All rights reserved.
//
import Foundation
import Alamofire

enum Router: URLRequestConvertible {
    case GetUserInfo([String: String])
    case PurchaseProduct([String: Any])
    
    var method: Alamofire.HTTPMethod {
        switch self {
        case .GetUserInfo:
            return .post
        case .PurchaseProduct:
            return .post
        }
    }
    
    var path: String {
        switch self {
        case .GetUserInfo:
            return Network.Endpoints.Login
        case .PurchaseProduct:
            if let id = UserDefaults.standard.object(forKey: "UserID") {
                return Network.Endpoints.Purchase + "\(id as! Int)"
            } else {
                return Network.Endpoints.Purchase
            }
        }
    }
    
    var parameters: [String: Any] {
        switch self {
        case .GetUserInfo(let params):
            return params 
        case .PurchaseProduct(let params):
            return params
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        var url : URL!
        switch self {
        case .GetUserInfo:
            url = URL(string: SettingsManager.sharedInstance.LoginBaseApiURL + path)
        case .PurchaseProduct:
            url = URL(string: SettingsManager.sharedInstance.PurchaseBaseApiURl + path)
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        let encodedURLRequest = try Alamofire.URLEncoding.default.encode(urlRequest, with: parameters)
        return encodedURLRequest
    }
}
