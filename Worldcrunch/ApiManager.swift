//
//  ApiManager.swift
//  Worldcrunch
//
//  Created by Miki Dimitrov on 11/28/16.
//  Copyright © 2016 hr.manifest.worldcrunch. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager {
    
    static let sharedInstance = ApiManager()
    
    func getUserInfo(email : String,password : String, completion: UserInfoCallBack){
        var id : Int!
        let currentUser = [Network.Parameters.email:email,Network.Parameters.password:password]
        Alamofire.request(Router.GetUserInfo(currentUser)).validate().responseJSON { response in
            switch response.result {
            case .success(_):
                if let JSON = response.result.value as? [String : Any] {
                    if let result = JSON["result"] as? [String : Any] {
                        if let user = result["user"] as? [String : Any] {
                            id = user["id"] as! Int
                        }
                    }
                }
                completion?(true, nil, User(emailAddress: email, password: password, id: id))
            case .failure(let error):
                completion?(false,error as? APIError,nil)
            }
        }
    }
    
    func userPurchase(identifier : String,delete : Bool,completion : PurchaseCallBack)  {
        var months = 0
        if identifier == Constants.productOneMonth || identifier == Constants.productFirstMonth {
            months = 1
        } else if identifier == Constants.productSixMonths {
            months = 6
        } else {
            months = 12
        }
        
        let params = [Network.Parameters.typeOffSubscription:months,Network.Parameters.tinyPass: 1]
        Alamofire.request(Router.PurchaseProduct(params)).responseJSON { response in
            switch response.result {
            case .success :
                completion!(true)
            case .failure :
                completion!(false)
            }
        }
    }
}
