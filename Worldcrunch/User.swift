//
//  User.swift
//  Worldcrunch
//
//  Created by Miki Dimitrov on 11/28/16.
//  Copyright © 2016 hr.manifest.worldcrunch. All rights reserved.
//
import Foundation

class User {
    var emailAddress : String!
    var password : String!
    var id : Int!
    
    init(emailAddress : String,password : String,id : Int) {
       self.emailAddress = emailAddress
       self.password = password
       self.id = id
    }
}
