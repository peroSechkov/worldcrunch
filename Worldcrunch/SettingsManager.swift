//
//  SettingManager.swift
//  Worldcrunch
//
//  Created by Miki Dimitrov on 11/28/16.
//  Copyright © 2016 hr.manifest.worldcrunch. All rights reserved.
//

import UIKit

class SettingsManager: NSObject {
    static let sharedInstance = SettingsManager()
    
    let LoginBaseApiURL: String
    let PurchaseBaseApiURl: String
    
    override init() {
        LoginBaseApiURL = "http://worldcrunch.com"
        PurchaseBaseApiURl = "http://dev2.worldcrunch.com"
        super.init()
    }
}
