//
//  NetworkConstants.swift
//  Worldcrunch
//
//  Created by Miki Dimitrov on 11/28/16.
//  Copyright © 2016 hr.manifest.worldcrunch. All rights reserved.
//

import Foundation
import UIKit

typealias UserInfoCallBack = ((_ success: Bool, _ error: APIError?, _ user: User?)-> ())?
typealias PurchaseCallBack = ((_ success: Bool)-> ())?

struct Network {
    struct Endpoints {
        static let Login = "/api/v1/auth/login"
        static let Purchase = "/api/v1/user/"
    }
    
    struct Parameters {
        static let email = "email"
        static let password = "password"
        static let typeOffSubscription = "typeof_sub"
        static let tinyPass = "tinypass"
        static let error = "error"
        static let message = "message"
    }
}

