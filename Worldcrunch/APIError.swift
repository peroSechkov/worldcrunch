//
//  APIError.swift
//  Worldcrunch
//
//  Created by Miki Dimitrov on 11/28/16.
//  Copyright © 2016 hr.manifest.worldcrunch. All rights reserved.
//

import UIKit

class APIError: NSObject {
    var error: NSError?
    var errorMessage: String?
    var errorDescription: String?
    
    static func errorFor(error: NSError?, data: NSData?) -> APIError {
        let apiError = APIError()
        apiError.error = error
        
        do {
            let JSON = try JSONSerialization.jsonObject(with: data! as Data, options: .allowFragments) as? NSDictionary
            apiError.errorDescription = JSON?[Network.Parameters.error] as? String
            apiError.errorMessage = JSON?[Network.Parameters.message] as? String
        } catch {
            apiError.errorDescription = "An error has occured"
            apiError.errorMessage = "Error"
        }
        return apiError
    }
}
